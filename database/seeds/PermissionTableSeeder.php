<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\User;
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           'user-list',
           'user-show',
           'user-create',
           'user-edit',
           'user-delete',
           'role-list',
           'role-create',
           'role-edit',
           'role-delete',
           'category-list',
           'category-create',
           'category-edit',
           'category-delete',
           'post-list',
           'post-create',
           'post-edit',
           'post-delete'
        ];


       foreach ($permissions as $permission) {
         Permission::create(['name' => $permission]);
       }

       $user = User::oldest()
                  ->first();

        if (!$user) {
          $user = User::create([
              'name' => 'administrator',
              'email' => 'admin@this.com',
              'password' => \Hash::make('11111111'),
          ]);
        }

        $role = Role::create(['name' => 'superadmin']);
        $role->syncPermissions($permissions);
        $user->assignRole('superadmin');


        // CREATE MANAGER ROLE
        $permissions = [
           'category-list',
           'category-create',
           'category-edit',
           'category-delete',
           'post-list',
           'post-create',
           'post-edit',
           'post-delete'
        ];

        $role = Role::create(['name' => 'manager']);
        $role->syncPermissions($permissions);

        // CREATE USER ROLE
        $permissions = [
           'category-list',
           'post-list'
        ];

        $role = Role::create(['name' => 'user']);
        $role->syncPermissions($permissions);



    }
}
