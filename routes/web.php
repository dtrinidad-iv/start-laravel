<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
});


//============================================ ROUTES =====================================================
// BASIC ROUTING
Route::get('/contact', function () {
    return view('contact');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/services', function () {
    return view('services');
});

// ROUTE PARAMETERS
// Reguired Parameters
Route::get('/post/{id}', function ($id) {
    return "the value of {id} is " . $id;
});

// Optional Parameters
Route::get('/user/{name?}', function ($name=null) {
    return "the value of {name} is " . $name;
});

// NAMED ROUTES
Route::get('settings/profile', function () {

  // Generating URLs...
  $url = route('profile');

  return $url;

  // Generating Redirects...
  // return redirect()->route('user.profile','Bill');

  // php artisan route:list

})->name('profile');

//============================================ CONTROLLERS =====================================================
// Route::get('categories','CategoriesController@index');
// Route::get('categories/{id}','CategoriesController@index');
Route::resource('pages','PageController');
// php artisan route:list
// App\Http\Middleware\VerifyCsrfToken @ $except = []; postman test


//============================================ VIEWS ===========================================================
Route::get('categories-page','PageController@categories_page');
Route::get('categories-page-show/{id}/{name}','PageController@categories_page_show');



//============================================ ELOQUENT ===========================================================
Route::resource('categories','CategoryController');
Route::resource('posts','PostController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//============================================ ROLE MGMT ===========================================================
Route::resource('roles','RoleController');
Route::resource('users','UserController');
