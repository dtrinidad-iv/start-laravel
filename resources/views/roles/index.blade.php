@extends('layouts.app')


@section('title', 'Role Management')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Role Management</h2>
            @can('role-create')
              <a class="btn btn-success" href="{{ route('roles.create') }}"> Create New Role</a>
            @endcan
            <hr>
        </div>
    </div>
</div>


@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif


<table id="rolesTable" class="table table-bordered">
  <thead>
    <tr>
       <th>Name</th>
       <th width="280px">Action</th>
    </tr>
  </thead>

  <tbody>
    @foreach ($roles as $key => $role)
    <tr>
        <td>{{ $role->name }}</td>
        <td>
            <a class="btn btn-info btn-sm" href="{{ route('roles.show',$role->id) }}">Show</a>
            @can('role-edit')
                <a class="btn btn-primary btn-sm" href="{{ route('roles.edit',$role->id) }}">Edit</a>
            @endcan
            @can('role-delete')
                {!! Form::open(['method' => 'DELETE', 'id' => 'formDelete', 'route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm submitDelete']) !!}
                {!! Form::close() !!}
            @endcan
        </td>
    </tr>
    @endforeach
  </tbody>

</table>

@endsection

@section('script')
  <script type="text/javascript">

    $(document).ready( function () {
        $('#rolesTable').DataTable();
    } );

    $('.submitDelete').on('click', function(e){
      e.preventDefault();
      Swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this data!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it'
      }).then((result) => {
        if (result.value) {

          $('#formDelete').submit();

          Swal(
            'Deleted!',
            'Your imaginary file has been deleted.',
            'success'
          )

        // For more information about handling dismissals please visit
        // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal(
            'Cancelled',
            'Your data is safe :)',
            'error'
          )
        }
      })
    });

  </script>

@endsection
