@extends('layouts.app')

@section('title','Users Management')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Users Management</h2>

              @can('user-create')
                <a class="btn btn-success" href="{{ route('users.create') }}"> Create New User</a>
              @endcan
               <hr>
        </div>


    </div>
</div>


@if ($message = Session::get('success'))
<div class="alert alert-success">
  <p>{{ $message }}</p>
</div>
@endif


<table id="usersTable" class="table table-bordered">
  <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Roles</th>
      <th width="280px">Action</th>
    </tr>
  </thead>

 @foreach ($data as $key => $user)
   <tbody>
     <tr>
       <td>{{ $user->name }}</td>
       <td>{{ $user->email }}</td>
       <td>
         @if(!empty($user->getRoleNames()))
           @foreach($user->getRoleNames() as $v)
              <label class="badge badge-success">{{ $v }}</label>
           @endforeach
         @endif
       </td>
       <td>
          @can('user-show')
            <a class="btn btn-info btn-sm" href="{{ route('users.show',$user->id) }}">Show</a>
          @endcan
          @can('user-edit')
            <a class="btn btn-primary btn-sm" href="{{ route('users.edit',$user->id) }}">Edit</a>
          @endcan
          @can('user-delete')
            {!! Form::open(['method' => 'DELETE', 'id' => 'formDelete', 'route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm submitDelete']) !!}
            {!! Form::close() !!}
          @endcan
       </td>
     </tr>
   </tbody>
 @endforeach
</table>



@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready( function () {
        $('#usersTable').DataTable();
    } );

    $('.submitDelete').on('click', function(e){
      e.preventDefault();
      Swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this data!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it'
      }).then((result) => {
        if (result.value) {

          $('#formDelete').submit();

          Swal(
            'Deleted!',
            'Your imaginary file has been deleted.',
            'success'
          )

        // For more information about handling dismissals please visit
        // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal(
            'Cancelled',
            'Your data is safe :)',
            'error'
          )
        }
      })
    });

  </script>

@endsection

