@extends('layouts.app')

@section('title', 'Eloquent Create')

@section('content')

    <!-- Page Content -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h1>Create Post</h1>
            {!! Form::open(['method' => 'POST', 'route' => 'posts.store', 'class' => 'form-horizontal']) !!}

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label('title', 'Title') !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    <small class="text-danger">{{ $errors->first('title') }}</small>
                </div>

                <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                    {!! Form::label('user_id', 'User') !!}
                    {!! Form::select('user_id', $users, null , ['class' => 'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('user_id') }}</small>
                </div>

                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                    {!! Form::label('body', 'Body') !!}
                    {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
                    <small class="text-danger">{{ $errors->first('body') }}</small>
                </div>

                <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                    {!! Form::label('category_id', 'Category') !!}
                    {!! Form::select('category_id', $categories, null, ['class' => 'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('category_id') }}</small>
                </div>

                <div class="btn-group pull-right">
                    {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
                    {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
                </div>
            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </section>


@endsection
