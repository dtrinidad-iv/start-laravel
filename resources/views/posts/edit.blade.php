@extends('layouts.app')

@section('title', 'Eloquent Create')

@section('content')

    <!-- Page Content -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h1>Edit Post</h1>
            {!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT']) !!}

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    {!! Form::label('title', 'Title') !!}
                    {!! Form::text('title', $post->title, ['class' => 'form-control']) !!}
                    <small class="text-danger">{{ $errors->first('title') }}</small>
                </div>

                <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                    {!! Form::label('user_id', 'User') !!}
                    {!! Form::select('user_id', $users, $post->user->id, ['class' => 'form-control']) !!}
                    <small class="text-danger">{{ $errors->first('user_id') }}</small>
                </div>

                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                    {!! Form::label('body', 'Body') !!}
                    {!! Form::textarea('body', $post->body , ['class' => 'form-control']) !!}
                    <small class="text-danger">{{ $errors->first('body') }}</small>
                </div>

                <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                    {!! Form::label('category_id', 'Categories') !!}
                    {!! Form::select('category_id', $categories, $post->category_id, ['class' => 'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('category_id') }}</small>
                </div>

                <div class="btn-group pull-right">

                    {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
                </div>

            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </section>


@endsection
