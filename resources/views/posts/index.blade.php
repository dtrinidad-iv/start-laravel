@extends('layouts.app')

@section('title', 'Eloquent')

@section('content')

    <!-- Page Content -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h1>Posts
              @can('post-create')
                <a href="posts/create" class="btn btn-info">Create</a>
              @endcan
            </h1>

            @if (Session::has('success'))
              <div class="alert alert-success">
                <p>{{Session::get('success') }}</p>
              </div>
            @endif
            @if (Session::has('failed'))
              <div class="alert alert-danger">
                <p>{{Session::get('failed') }}</p>
              </div>
            @endif

            <ul>
              <table id="postsTable" class="table">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Body</th>
                    <th>User</th>
                    <th>Category</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                    @foreach ($posts as $post)
                    <tr>
                      <td>{{$post->title}}</td>
                      <td>{{$post->body}}</td>
                      <td>{{$post->user->name}}</td>
                      <td>{{$post->category->name}}</td>
                      <td>
                        @can('post-edit')
                          <a style="float:left;margin-right:4px;" href="posts/{{$post->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                        @endcan
                        @can('post-delete')
                        {!! Form::model($post, ['route' => ['posts.destroy', $post->id], 'method' => 'DELETE' , 'id' => 'formDelete']) !!}
                            <div style="float:left;" class="btn-group">
                                {!! Form::submit("Delete", ['class' => 'btn btn-sm btn-danger submitDelete']) !!}
                            </div>
                        {!! Form::close() !!}
                        @endcan
                      </td>
                    </tr>
                    @endforeach

                </tbody>
              </table>


            </ul>
          </div>
        </div>
      </div>
    </section>


@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready( function () {
        $('#postsTable').DataTable();
    } );

    $('.submitDelete').on('click', function(e){
      e.preventDefault();
      Swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this data!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it'
      }).then((result) => {
        if (result.value) {

          $('#formDelete').submit();

          Swal(
            'Deleted!',
            'Your imaginary file has been deleted.',
            'success'
          )

        // For more information about handling dismissals please visit
        // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal(
            'Cancelled',
            'Your data is safe :)',
            'error'
          )
        }
      })
    });

  </script>

@endsection
