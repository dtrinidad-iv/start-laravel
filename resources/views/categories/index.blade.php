@extends('layouts.app')

@section('title', 'Categories')

@section('content')

    <!-- Page Content -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h1>Categories

            @can('category-create')
              <a href="categories/create" class="btn btn-info">Create</a>
            @endcan

            @can('post-create')
              <a href="posts" class="btn btn-primary">Posts</a>
            @endcan

          </h1>

            @if (Session::has('success'))
              <div class="alert alert-success">
                <p>{{Session::get('success') }}</p>
              </div>
            @endif
            @if (Session::has('failed'))
              <div class="alert alert-danger">
                <p>{{Session::get('failed') }}</p>
              </div>
            @endif

            <ul>
              <table id="categoriesTable" class="table">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                    @foreach ($categories as $category)
                    <tr>
                      <td>{{$category->name}}</td>
                      <td>{{$category->description}}</td>
                      <td>

                        @can('category-edit')
                          <a style="float:left;margin-right:4px;" href="categories/{{$category->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                        @endcan

                        @can('category-delete')
                        {!! Form::model($category, ['route' => ['categories.destroy', $category->id], 'method' => 'DELETE' , 'id' => 'formDelete']) !!}
                            <div style="float:left;" class="btn-group">
                                {!! Form::submit("Delete", ['class' => 'btn btn-sm btn-danger submitDelete']) !!}
                            </div>
                        {!! Form::close() !!}
                        @endcan
                      </td>
                    </tr>
                    @endforeach

                </tbody>
              </table>


            </ul>
          </div>
        </div>
      </div>
    </section>


@endsection

@section('script')
  <script type="text/javascript">
    $(document).ready( function () {
        $('#categoriesTable').DataTable();
    } );

    $('.submitDelete').on('click', function(e){
      e.preventDefault();
      Swal({
        title: 'Are you sure?',
        text: 'You will not be able to recover this data!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, keep it'
      }).then((result) => {
        if (result.value) {

          $('#formDelete').submit();

          Swal(
            'Deleted!',
            'Your imaginary file has been deleted.',
            'success'
          )

        // For more information about handling dismissals please visit
        // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal(
            'Cancelled',
            'Your data is safe :)',
            'error'
          )
        }
      })
    });

  </script>

@endsection
