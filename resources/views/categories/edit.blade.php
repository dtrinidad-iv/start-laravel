@extends('layouts.app')

@section('title', 'Categories Create')

@section('content')

    <!-- Page Content -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h1>Categories Edit</h1>
            {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'PUT']) !!}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', $category->name , ['class' => 'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('name') }}</small>
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::textarea('description', $category->description, ['class' => 'form-control']) !!}
                    <small class="text-danger">{{ $errors->first('description') }}</small>
                </div>

                <div class="btn-group pull-right">
                    {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
                    {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
                </div>

            {!! Form::close() !!}
          </div>
        </div>

          <hr>
          <h1>POSTS</h1>
          @foreach ($category->posts as $post)
            <h3>{{$post->title}}</h3>
            <p>{{$post->body}}</p>
            <hr>
          @endforeach
      </div>
    </section>


@endsection
