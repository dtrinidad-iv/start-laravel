@extends('layouts.app')

@section('title', 'Categories Create')

@section('content')

    <!-- Page Content -->
    <section>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h1>Categories Create</h1>
            {!! Form::open(['method' => 'POST', 'route' => 'categories.store', 'class' => 'form-horizontal']) !!}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::label('name', 'Name') !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    <small class="text-danger">{{ $errors->first('name') }}</small>
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    {!! Form::label('description', 'Description') !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    <small class="text-danger">{{ $errors->first('description') }}</small>
                </div>

                <div class="btn-group pull-right">
                    {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
                    {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
                </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </section>


@endsection
