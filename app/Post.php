<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $primaryKey  = 'id';
    public $timestamps = true;

   public function user()
   {
       return $this->hasOne('App\User', 'id', 'user_id');
   }

   public function category()
   {
       return $this->belongsTo('App\Category','category_id','id');
   }

}
