<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Validator;

class CategoryApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all()->toArray();

        $response = [
          'success' => true,
          'data' => $categories,
          'message' => 'Retrieve Categories Successfully'
        ];

        return response()->json($response,200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $validator = Validator::make($request->all(), [
          'name' => 'required|unique:categories'
       ]);


       if($validator->fails()){

           $response = [
             'success' => false,
             'data' => $validator->errors(),
             'message' => 'Validation Error'
           ];

           return response()->json($response,400);
       }

       $category = new Category;
       $category->name = $request->name;
       $category->description = $request->description;
       $category->save();


       $response = [
         'success' => true,
         'data' => $category,
         'message' => 'Created Category Successfully'
       ];

       return response()->json($response,200);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $category = Category::find($id);

      if (is_null($category)) {

        $response = [
          'success' => false,
          'data' => [],
          'message' => 'Category not found'
        ];

        return response()->json($response,400);

      }

      $response = [
        'success' => true,
        'data' => $category->toArray(),
        'message' => 'Retrieved Category Successfully'
      ];

      return response()->json($response,200);

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $validator = Validator::make($request->all(), [
        'name' => 'required|unique:categories,name,'.$id
      ]);


      if($validator->fails()){

          $response = [
            'success' => false,
            'data' => $validator->errors(),
            'message' => 'Validation Error'
          ];

          return response()->json($response,400);
      }


      $category = Category::find($id);

      if (is_null($category)) {

        $response = [
          'success' => false,
          'data' => [],
          'message' => 'Category not found'
        ];

        return response()->json($response,400);

      }

      $category->name = $request->name;
      $category->description = $request->description;
      $category->save();

      $response = [
        'success' => true,
        'data' => $category,
        'message' => 'Updated Category Successfully'
      ];

      return response()->json($response,200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $category = Category::find($id);

      if (is_null($category)) {

        $response = [
          'success' => false,
          'data' => [],
          'message' => 'Category not found'
        ];

        return response()->json($response,400);

      }

      $category->delete();


      $response = [
        'success' => true,
        'data' => $category,
        'message' => 'Deleted Category Successfully'
      ];

      return response()->json($response,200);

    }

}
