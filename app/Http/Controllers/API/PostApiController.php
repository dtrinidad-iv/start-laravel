<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use Validator;

class PostApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all()->toArray();

        $response = [
          'success' => true,
          'data' => $posts,
          'message' => 'Retrieve Posts Successfully'
        ];

        return response()->json($response,200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $validator = Validator::make($request->all(), [
         'title' => 'required',
         'body' => 'required',
         'category_id' => 'required',
         'user_id' => 'required'
       ]);


       if($validator->fails()){

           $response = [
             'success' => false,
             'data' => $validator->errors(),
             'message' => 'Validation Error'
           ];

           return response()->json($response,400);
       }

       try {

         $post = new Post;
         $post->title = $request->title;
         $post->body = $request->body;
         $post->user_id = $request->user_id;
         $post->category_id = $request->category_id;
         $post->save();

       } catch (\Exception $e) {

         $response = [
           'success' => false,
           'data' => $e->getMessage(),
           'message' => 'Exception Error'
         ];

         return response()->json($response,400);
       }



       $response = [
         'success' => true,
         'data' => $post,
         'message' => 'Created Post Successfully'
       ];

       return response()->json($response,200);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $post = Post::find($id);

      if (is_null($post)) {

        $response = [
          'success' => false,
          'data' => [],
          'message' => 'Post not found'
        ];

        return response()->json($response,400);

      }

      $response = [
        'success' => true,
        'data' => $post->toArray(),
        'message' => 'Retrieved Post Successfully'
      ];

      return response()->json($response,200);

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $validator = Validator::make($request->all(), [
        'title' => 'required',
        'body' => 'required',
        'category_id' => 'required',
        'user_id' => 'required'
      ]);


      if($validator->fails()){

          $response = [
            'success' => false,
            'data' => $validator->errors(),
            'message' => 'Validation Error'
          ];

          return response()->json($response,400);
      }


      $post = Post::find($id);

      if (is_null($post)) {

        $response = [
          'success' => false,
          'data' => [],
          'message' => 'Post not found'
        ];

        return response()->json($response,400);

      }

      try {
        $post->title = $request->title;
        $post->body = isset($request->body) ? $request->body : $post->body;
        $post->category_id = isset($request->category_id) ? $request->category_id : $post->category_id;
        $post->user_id = isset($request->user_id) ? $request->user_id : $post->user_id;
        $post->save();
      } catch (\Exception $e) {

        $response = [
          'success' => false,
          'data' => $e->getMessage(),
          'message' => 'Exception Error'
        ];

        return response()->json($response,400);

      }



      $response = [
        'success' => true,
        'data' => $post,
        'message' => 'Updated Post Successfully'
      ];

      return response()->json($response,200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $post = Post::find($id);

      if (is_null($post)) {

        $response = [
          'success' => false,
          'data' => [],
          'message' => 'Post not found'
        ];

        return response()->json($response,400);

      }

      $post->delete();


      $response = [
        'success' => true,
        'data' => $post,
        'message' => 'Deleted Post Successfully'
      ];

      return response()->json($response,200);

    }

}
