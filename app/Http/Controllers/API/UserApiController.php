<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserApiController extends Controller
{
  /**
    * login api
    *
    * @return \Illuminate\Http\Response
    */
   public function login(){

       if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
           $user = Auth::user();
           $user['token'] =  $user->createToken('MyApp')->accessToken;
           $response = [
             'success' => true,
             'data' => $user,
             'message' => 'Login Successfully'
           ];

           return response()->json($response,200);
       }
       else{

           $response = [
             'success' => false,
             'message' => 'Unauthorized'
           ];

           return response()->json($response,401);

       }
   }


   /**
    * Register api
    *
    * @return \Illuminate\Http\Response
    */
   public function register(Request $request)
   {


       $validator = Validator::make($request->all(), [
           'name' => 'required',
           'email' => 'required|email|unique:users',
           'password' => 'required',
           'c_password' => 'required|same:password',
       ]);


       if ($validator->fails()) {


           $response = [
             'success' => false,
             'data' => $validator->errors(),
             'message' => 'Validation Error'
           ];

           return response()->json($response,400);

       }


       $input = $request->all();
       $input['password'] = bcrypt($input['password']);
       $user = User::create($input);
       $user['token'] =  $user->createToken('MyApp')->accessToken;
       $response = [
         'success' => true,
         'data' => $user,
         'message' => 'Register Successfully'
       ];

       return response()->json($response,200);
   }



}
